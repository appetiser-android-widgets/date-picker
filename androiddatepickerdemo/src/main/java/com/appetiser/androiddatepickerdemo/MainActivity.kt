package com.appetiser.androiddatepickerdemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.appetiser.androiddatepicker.AppetiserDatePicker
import java.util.*

class MainActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<AppetiserDatePicker>(R.id.datePicker)
            .apply {
                setOnDateSelectedListener {
                    Log.d(TAG, "onCreate: $it")
                }
                setDefaultPickerDate(Calendar.getInstance().time)
                setDobText("select date")
                setHintText("select date hint")
                setHintTextColor(
                    ContextCompat
                        .getColor(
                            this@MainActivity,
                            android.R.color.holo_green_dark
                        )
                )
                setLabelText("birth date")
                setLabelTextColor(
                    ContextCompat
                        .getColorStateList(
                            this@MainActivity,
                            R.color.date_picker_label_text_color
                        )!!
                )
                setTextColor(
                    ContextCompat
                        .getColor(
                            this@MainActivity,
                            android.R.color.darker_gray
                        )
                )
                setUnderLineBackground(
                    ContextCompat
                        .getDrawable(
                            this@MainActivity,
                            R.drawable.date_picker_underline_background
                        )!!
                )
                setDpMainDialogColor(
                    ContextCompat
                        .getColor(
                            this@MainActivity,
                            android.R.color.holo_orange_dark
                        )
                )
            }
    }
}
